/**
 * This file provides a GameMaker compatible API for reading properties of the
 * current display.
 */
#include "stdafx.h"

/// The required function signature for DLL entrypoints to be usable by GameMaker.
#define GMEXPORT extern "C" __declspec(dllexport)

/**
 * Gets the settings for the current display.
 *
 * @param deviceMode The structure to populate with the display settings.
 */
void getDisplaySettings(DEVMODEA& deviceMode) {
    deviceMode.dmSize = sizeof(deviceMode);
    deviceMode.dmDriverExtra = 0;

    // This method can fail, but only when requesting an explicit display index. As
    // it cannot fail when requesting the current display, the return value is not
    // important.
    (void) EnumDisplaySettingsA(nullptr, ENUM_CURRENT_SETTINGS, &deviceMode);
}

/**
 * Gets the display flags (greyscale/progressive).
 *
 * Note:  The greyscale flag is no longer valid.
 *
 * @return the display flags for the current display.
 */
DWORD getFlags() {
    DEVMODEA deviceMode;

    getDisplaySettings(deviceMode);

    return deviceMode.dmDisplayFlags;
}

/**
 * Gets the number of bits per pixel used to specify colours on the display.
 *
 * @return The number of bits per pixel used on the current display.
 */
GMEXPORT double getBitsPerPixel() {
    DEVMODEA deviceMode;

    getDisplaySettings(deviceMode);

    return deviceMode.dmBitsPerPel;
}

/**
 * Gets the height of the display.
 *
 * @return The height of the display (in pixels).
 */
GMEXPORT double getHeight() {
    DEVMODEA deviceMode;

    getDisplaySettings(deviceMode);

    return deviceMode.dmPelsHeight;
}

/**
 * Gets the current refresh rate.
 *
 * @return The current refresh rate (in hertz).
 */
GMEXPORT double getRefreshRate() {
    DEVMODEA deviceMode;

    getDisplaySettings(deviceMode);

    return deviceMode.dmDisplayFrequency;
}

/**
 * Gets the width of the display.
 *
 * @return The width of the display (in pixels).
 */
GMEXPORT double getWidth() {
    DEVMODEA deviceMode;

    getDisplaySettings(deviceMode);

    return deviceMode.dmPelsWidth;
}

/**
 * Indicates whether this is an interlaced or progressive display.
 *
 * @retval 0    This is a progressive display.
 * @retval 1    This is an interlaced display.
 */
GMEXPORT double isInterlaced() {
    DWORD flags = getFlags();

    return (flags & DM_INTERLACED) != 0;
}

/**
 * Indicates whether this is an interlaced or progressive display.
 *
 * @retval 0    This is an interlaced display.
 * @retval 1    This is a progressive display.
 */
GMEXPORT double isProgressive() {
    return !isInterlaced();
}
