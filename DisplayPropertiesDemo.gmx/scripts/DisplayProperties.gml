#define DisplayProperties


#define display_getBitsPerPixel
/// display_getBitsPerPixel()
///
/// Gets the number of bits per pixel used to specify colours on the display.
///
/// Returns:
///     The number of bits per pixel used on the current display.

with (oDisplayProperties) {
    return external_call(fnGetBitsPerPixel);
}

#define display_getHeight
/// display_getHeight()
///
/// Gets the height of the display.
///
/// Returns:
///     The height of the display (in pixels).

with (oDisplayProperties) {
    return external_call(fnGetHeight);
}

#define display_getRefreshRate
/// display_getRefreshRate()
///
/// Gets the current refresh rate.
///
/// Returns:
///     The current refresh rate (in hertz).

with (oDisplayProperties) {
    return external_call(fnGetRefreshRate);
}

#define display_getWidth
/// display_getWidth()
///
/// Gets the width of the display.
///
/// Returns:
///     The width of the display (in pixels).

with (oDisplayProperties) {
    return external_call(fnGetWidth);
}

#define display_isInterlaced
/// display_isInterlaced()
///
/// Indicates whether this is an interlaced or progressive display.
///
/// Returns:
///     0    This is a progressive display.
///     1    This is an interlaced display.

with (oDisplayProperties) {
    return external_call(fnIsInterlaced);
}

#define display_isProgressive
/// display_isProgressive()
///
/// Indicates whether this is an interlaced or progressive display.
///
/// Returns:
///     0    This is an interlaced display.
///     1    This is a progressive display.

with (oDisplayProperties) {
    return external_call(fnIsProgressive);
}
